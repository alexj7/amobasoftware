export default {
  SET_USER(state, users) {
    state.users = users;
  },
  CREATE_USER(state, user) {
    state.users.push(user);
  },
  UPDATE_USERS(state, user) {
    const UsersIndex = state.users.findIndex((p) => p.id === user.id);
    Object.assign(state.users[UsersIndex], user);
  },
  REMOVE_USER(state, itemId) {
    const ItemIndex = state.users.findIndex((p) => p.id === itemId);
    state.users.splice(ItemIndex, 1);
  },
  OPEN_MODAL(state) {
    state.open = true;
  },
  CLOSE_MODAL(state) {
    if(state.open){
      state.open = false;
    }
  },
};
