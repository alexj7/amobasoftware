import { db } from "../../firebase/firebaseConfig";
import firebase from "firebase";

export default {
  // Fetch emails
  getUser({ commit }) {
    return new Promise((resolve, reject) => {
      const users = [];
      db.collection("users")
        .get()
        .then((response) => {
          response.forEach((res) => {
            const data = res.data();
            const user = {
              id: res.id,
              name: data.name,
              description: data.description,
              image: data.image,
            };
            users.push(user);
          });
          commit("SET_USER", users);
          resolve(users);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  createUser({ commit }, item) {
    let imageUrl
    let id
    return new Promise((resolve, reject) => {
      db.collection("users")
        .add(item.user)
        .then((response) => {
          if(!item.image){
            commit("CREATE_USER", item.user);
            resolve(response);
          }
          id = response.id
          return response.id
        }).then( id => {
            const filename = item.image.name
            const ext = filename.slice(filename.lastIndexOf('.'))
            return firebase.storage().ref(`user/${id}${ext}`).put(item.image)

        }).then( async fileData  => {
          imageUrl = await fileData.ref.getDownloadURL()
          console.log(id)
          item.user.image = imageUrl
          return db.collection('users').doc(id).update({
            image: imageUrl
          })
        }).then( data => {
          commit("CREATE_USER", item.user);
          resolve(data);

        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  updateUser({ commit }, item) {
    return new Promise((resolve, reject) => {
      db.collection("users")
        .doc(item.id)
        .update({
          name: item.name,
          description: item.description,
          image: item.image
        })
        .then((response) => {
          console.log('item', item)
          commit("UPDATE_USERS", item);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  removeUser({ commit }, itemId) {
    return new Promise((resolve, reject) => {
      db.collection("users")
        .doc(itemId)
        .delete()
        .then((response) => {
          commit("REMOVE_USER", itemId);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  openModal({ commit }) {
    commit("OPEN_MODAL");
  },
  closeModal({ commit }) {
    commit("CLOSE_MODAL");
  },
};
