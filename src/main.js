import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";
import router from "./router";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import { store } from "./vuex/vuex";
import Notifications from "vue-notification";
import VueAxios from 'vue-axios'
import axios from 'axios'
// Firebase
import "@/firebase/firebaseConfig";

Vue.config.productionTip = false;

// Install vuex
Vue.use(Vuex);
// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
//Install axios
Vue.use(VueAxios, axios)
/*
or for SSR:
import Notifications from 'vue-notification/dist/ssr.js'
*/
Vue.use(Notifications);

new Vue({
  router,
  render: (h) => h(App),
  store,
}).$mount("#app");
