import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCbTHne7FIE9f2cwC_6T_730tEMTQnhuLY",
  projectId: "prueba-36bef",
  appId: "1:532177632866:web:38805d861f21bb8389db3b",
  storageBucket: 'gs://prueba-36bef.appspot.com'
};

firebase.initializeApp(firebaseConfig);
// Initialize Firebase
const firebaseFirestore = firebase.firestore();

const db = firebaseFirestore;
export { firebaseFirestore, db };
